package pe.edu.idat.proyectofinal.bean;

import pe.edu.idat.proyectofinal.model.ProvinciaModel;

public class ProvinciaBean extends ProvinciaModel {

    public ProvinciaBean() {
    }

    public ProvinciaBean(int prov_id, String prov_desc) {
        super(prov_id, prov_desc);
    }
}
