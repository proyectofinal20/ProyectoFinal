package pe.edu.idat.proyectofinal.model;

public abstract class PatrimonioModel {
    String ped_codigo;
    String ped_fecemi;
    String ped_clicod;
    int ped_estado;
    String ped_artcod;
    int ped_cantidad;
    Double ped_precio;
    int ped_estadodet;

    public PatrimonioModel() {
    }

    public PatrimonioModel(String ped_codigo, String ped_fecemi, String ped_clicod, int ped_estado, String ped_artcod, int ped_cantidad, Double ped_precio, int ped_estadodet) {
        this.ped_codigo = ped_codigo;
        this.ped_fecemi = ped_fecemi;
        this.ped_clicod = ped_clicod;
        this.ped_estado = ped_estado;
        this.ped_artcod = ped_artcod;
        this.ped_cantidad = ped_cantidad;
        this.ped_precio = ped_precio;
        this.ped_estadodet = ped_estadodet;
    }

    public String getPed_codigo() {
        return ped_codigo;
    }

    public void setPed_codigo(String ped_codigo) {
        this.ped_codigo = ped_codigo;
    }

    public String getPed_fecemi() {
        return ped_fecemi;
    }

    public void setPed_fecemi(String ped_fecemi) {
        this.ped_fecemi = ped_fecemi;
    }

    public String getPed_clicod() {
        return ped_clicod;
    }

    public void setPed_clicod(String ped_clicod) {
        this.ped_clicod = ped_clicod;
    }

    public int getPed_estado() {
        return ped_estado;
    }

    public void setPed_estado(int ped_estado) {
        this.ped_estado = ped_estado;
    }

    public String getPed_artcod() {
        return ped_artcod;
    }

    public void setPed_artcod(String ped_artcod) {
        this.ped_artcod = ped_artcod;
    }

    public int getPed_cantidad() {
        return ped_cantidad;
    }

    public void setPed_cantidad(int ped_cantidad) {
        this.ped_cantidad = ped_cantidad;
    }

    public Double getPed_precio() {
        return ped_precio;
    }

    public void setPed_precio(Double ped_precio) {
        this.ped_precio = ped_precio;
    }

    public int getPed_estadodet() {
        return ped_estadodet;
    }

    public void setPed_estadodet(int ped_estadodet) {
        this.ped_estadodet = ped_estadodet;
    }
}
