package pe.edu.idat.proyectofinal;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;

import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONObject;

import java.util.List;
import java.util.Locale;

import pe.edu.idat.proyectofinal.sharedpreferences.MarkerMapClass;


public class FrgMapaMarker extends Fragment implements OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnMyLocationClickListener{
    private MapView mapView;
    private GoogleMap mMap;
    private static final String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";
    LatLng ny, nj;
    private View popup = null;
    Marker marker;
    Double latitud = Double.valueOf(0);
    Double longitud = Double.valueOf(0);
    LatLng latLngVerify;
    MarkerOptions markerOptions = null;

    public static FrgMapaMarker newInstance() {
        FrgMapaMarker fragment = new FrgMapaMarker();
        return fragment;
    }

    public FrgMapaMarker(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_frg_mapa_marker, container, false);
        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY);
        }

        mapView = v.findViewById(R.id.mapView);
        mapView.onCreate(mapViewBundle);
        mapView.getMapAsync(this);
        latLngVerify = new LatLng(latitud,longitud);
        //referencias
        FloatingActionButton fab = v.findViewById(R.id.fab_map_check);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.i("LATLNG - COMPROBAR", String.valueOf(latLngVerify.latitude));
                if(String.valueOf(latLngVerify.latitude) == "0.0"){
                    Toast.makeText(getActivity(),"Por favor selecciona una ubicación",Toast.LENGTH_LONG).show();
                }else{
                    try {
                        String[] arrayLatlng = markerOptions.getSnippet().split(":");
                        String lat = null;
                        String log = null;
                        for (int i = 0; i < arrayLatlng.length; i++) {
                            lat = arrayLatlng[0];
                            log = arrayLatlng[1];
                        }
                        Log.i("latlng",lat.toString()+"--"+log.toString());
                        JSONObject jsonParam = new JSONObject();
                        jsonParam.put("marker_title", markerOptions.getTitle());
                        jsonParam.put("marker_latitud", lat.toString());
                        jsonParam.put("marker_longitud", log.toString());
                        Log.e("params",jsonParam.toString());
                        //Enviar al shared
                        MarkerMapClass.savePreferences(getActivity(),"dataMarkerUser",jsonParam.toString());
                        MapContentActivity mapContentActivity = new MapContentActivity();
                        getActivity().onBackPressed();
                    }catch (Exception e){
                        Log.i("ERROR MAPA",e.getMessage());

                    }
                }

            }
        });

        return v;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(MAP_VIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAP_VIEW_BUNDLE_KEY, mapViewBundle);
        }

        mapView.onSaveInstanceState(mapViewBundle);
    }



    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onPause() {
        mapView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMinZoomPreference(12);
        mMap.setIndoorEnabled(true);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        UiSettings uiSettings = mMap.getUiSettings();
        uiSettings.setIndoorLevelPickerEnabled(true);
        uiSettings.setMyLocationButtonEnabled(true);
        uiSettings.setMapToolbarEnabled(true);
        uiSettings.setCompassEnabled(true);
        uiSettings.setZoomControlsEnabled(true);
        uiSettings.setZoomGesturesEnabled(true);
        uiSettings.setCompassEnabled(true);
        uiSettings.setRotateGesturesEnabled(true);

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                mMap.clear();
                latLngVerify = latLng;
                String dire = "";
                try {
                    Geocoder geo = new Geocoder(getActivity().getApplicationContext(), Locale.getDefault());
                    List<Address> addresses = geo.getFromLocation(latLng.latitude, latLng.longitude, 1);
                    if (addresses.isEmpty()) {
                    }
                    else {
                        if (addresses.size() > 0) {
                            //dire = String.valueOf(addresses.get(0).getFeatureName() + "\n " + addresses.get(0).getLocality() +"\n" + addresses.get(0).getAdminArea() + "\n" + addresses.get(0).getCountryName());
                            dire = String.valueOf(addresses.get(0).getAddressLine(0));
                        }
                    }
                }
                catch (Exception e) {
                    e.printStackTrace(); // getFromLocation() may sometimes fail
                }
                markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title(dire.toString());
                markerOptions.snippet(latLng.latitude + " : " + latLng.longitude);
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker));

                mMap.addMarker(markerOptions).showInfoWindow();
            }
        });


        // Setting a custom info window adapter for the google map
        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            // Use default InfoWindow frame
            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            // Defines the contents of the InfoWindow
            @Override
            public View getInfoContents(Marker arg0) {
                View v = null;
                try {

                    // Getting view from the layout file info_window_layout
                    v = getLayoutInflater().inflate(R.layout.custom_infowindow, null);

                    // Getting reference to the TextView to set latitude
                    TextView addressTxt = (TextView) v.findViewById(R.id.addressTxt);
                    addressTxt.setText(arg0.getTitle());

                } catch (Exception ev) {
                    System.out.print(ev.getMessage());
                }

                return v;
            }
        });



        /*Double lat1 = Double.parseDouble("-12.1243056");
        Double longitud1 = Double.parseDouble("-77.0300062");
        ny = new LatLng(lat1, longitud1);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(ny);
        markerOptions.title("IDAT").snippet("Instituto Tecnológico");
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_pin_maps));
        mMap.addMarker(markerOptions);

        mMap.moveCamera(CameraUpdateFactory.newLatLng(ny));*/
        Double lat1 = Double.parseDouble("-12.1243056");
        Double longitud1 = Double.parseDouble("-77.0300062");
        ny = new LatLng(lat1, longitud1);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(ny));
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
        Toast.makeText(getActivity(), "Current location:\n" + location, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onMyLocationButtonClick() {
        Toast.makeText(getActivity(), "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }



}
