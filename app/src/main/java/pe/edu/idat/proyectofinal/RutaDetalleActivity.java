package pe.edu.idat.proyectofinal;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pe.edu.idat.proyectofinal.controller.RutaController;
import pe.edu.idat.proyectofinal.controller.RutasController;

public class RutaDetalleActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnMyLocationClickListener {

    RutasController rutasController=new RutasController();
    private GoogleMap mMap;
    LatLng nj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ruta_detalle);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        //String datorecibido = getIntent().getExtras().getString("rutaFecha");
        new ListarRepartos().execute();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMinZoomPreference(12);
        mMap.setIndoorEnabled(true);
        if (ActivityCompat.checkSelfPermission(RutaDetalleActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(RutaDetalleActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        UiSettings uiSettings = mMap.getUiSettings();
        uiSettings.setIndoorLevelPickerEnabled(true);
        uiSettings.setMyLocationButtonEnabled(true);
        uiSettings.setMapToolbarEnabled(true);
        uiSettings.setCompassEnabled(true);
        uiSettings.setZoomControlsEnabled(true);
        uiSettings.setZoomGesturesEnabled(true);
        uiSettings.setCompassEnabled(true);
        uiSettings.setRotateGesturesEnabled(true);

       /* mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                DialogRuta dialog = new DialogRuta();
                dialog.show(getFragmentManager(), "RUTA");
                return true;
            }
        });
        */
    }

    private class ListarRepartos extends AsyncTask<String, Void, JSONObject> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(RutaDetalleActivity.this, "Cargando", "espere por favor");
            super.onPreExecute();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            return rutasController.ListarRutasReparto();
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            progressDialog.dismiss();
            try {
                if (jsonObject.getBoolean("status") && jsonObject.has("data")) {
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    Log.i("JSON MAPA", jsonArray.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        String rt_codigo = jsonArray.getJSONObject(i).getString("rt_codigo");
                        String rt_clicod = jsonArray.getJSONObject(i).getString("rt_clicod");
                        Double rt_latitud = jsonArray.getJSONObject(i).getDouble("rt_latitud");
                        Double rt_longitud = jsonArray.getJSONObject(i).getDouble("rt_longitud");
                        String rt_direc = jsonArray.getJSONObject(i).getString("rt_direc");
                        String rt_movil1 = jsonArray.getJSONObject(i).getString("rt_movil1");
                        String rt_movil2 = jsonArray.getJSONObject(i).getString("rt_movil2");

                        nj = new LatLng(rt_latitud, rt_longitud);
                        MarkerOptions markerOptions = new MarkerOptions();
                        markerOptions.position(nj);
                        markerOptions.title(rt_codigo);
                        markerOptions.snippet(jsonArray.toString());
                        markerOptions.snippet(jsonArray.get(i).toString());
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_pin_maps));
                        mMap.addMarker(markerOptions);
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(nj));

                        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                            @Override
                            public boolean onMarkerClick(Marker marker) {
                                try {
                                    DialogRuta myBottomSheetDialog = DialogRuta.getInstance(RutaDetalleActivity.this);
                                    myBottomSheetDialog.txt_codped(String.valueOf(marker.getTitle()));
                                    JSONObject json = new JSONObject(String.valueOf(marker.getSnippet()));
                                    myBottomSheetDialog.txt_direccion(json.getString("rt_direc"));
                                    myBottomSheetDialog.telf1(String.valueOf(json.getString("rt_movil1")));
                                    myBottomSheetDialog.telf2(String.valueOf(json.getString("rt_movil2")));
                                    myBottomSheetDialog.setCanceledOnTouchOutside(false);
                                    myBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                                    myBottomSheetDialog.show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                return true;
                            }
                        });

                        //fin 2
                        }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
    //fin

    @Override
    public void onMyLocationClick(@NonNull Location location) {
        Toast.makeText(RutaDetalleActivity.this, "Current location:\n" + location, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onMyLocationButtonClick() {
        Toast.makeText(RutaDetalleActivity.this, "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }


}
