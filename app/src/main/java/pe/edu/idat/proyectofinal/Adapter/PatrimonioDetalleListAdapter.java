package pe.edu.idat.proyectofinal.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import pe.edu.idat.proyectofinal.R;
import pe.edu.idat.proyectofinal.bean.PatrimonioDetalleBean;

public class PatrimonioDetalleListAdapter extends RecyclerView.Adapter<PatrimonioDetalleListAdapter.PatrimonioDetalleViewHolder> {
    //
    private Context context;
    private List<PatrimonioDetalleBean> patrimonios;
    private RecyclerView mRecyclerV;


    public PatrimonioDetalleListAdapter(List<PatrimonioDetalleBean> items, Context contexto, RecyclerView recyclerView) {
        this.context = contexto;
        this.patrimonios = items;
        this.mRecyclerV = recyclerView;
    }
    //

    @Override
    public PatrimonioDetalleViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.molde_articulos, viewGroup, false);
        return new PatrimonioDetalleViewHolder(v);
    }

    @Override
    public void onBindViewHolder(PatrimonioDetalleViewHolder viewHolder, int position) {
        final PatrimonioDetalleBean item = patrimonios.get(position);
        viewHolder.itemView.setTag(item);
        viewHolder.art.setText(item.getPed_artcod());
        viewHolder.cant.setText(String.valueOf(item.getPed_cantidad()));
        viewHolder.monto.setText(String.valueOf(item.getPed_precio()));
    }

    @Override
    public int getItemCount() {
        return patrimonios.size();
    }

    static class PatrimonioDetalleViewHolder extends RecyclerView.ViewHolder {
        protected TextView art;
        protected TextView cant;
        protected TextView monto;
        public View layout;

        public PatrimonioDetalleViewHolder(View v) {
            super(v);
            layout = v;
            this.art = (TextView) v.findViewById(R.id.molde_art_desc);
            this.cant = (TextView) v.findViewById(R.id.molde_art_cant);
            this.monto = (TextView) v.findViewById(R.id.molde_art_monto);
        }
    }

}
