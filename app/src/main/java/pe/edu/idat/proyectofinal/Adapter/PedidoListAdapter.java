package pe.edu.idat.proyectofinal.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import pe.edu.idat.proyectofinal.R;
import pe.edu.idat.proyectofinal.bean.PatrimonioBean;

public class PedidoListAdapter extends RecyclerView.Adapter<PedidoListAdapter.PedidoViewHolder> {
    //
    private Context context;
    private List<PatrimonioBean> patrimonios;
    private RecyclerView mRecyclerV;


    public PedidoListAdapter(List<PatrimonioBean> items, Context contexto, RecyclerView recyclerView) {
        this.context = contexto;
        this.patrimonios = items;
        this.mRecyclerV = recyclerView;
    }
    //

    @Override
    public PedidoViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.molde_patrimonio, viewGroup, false);
        return new PedidoViewHolder(v);
    }

    @Override
    public void onBindViewHolder(PedidoViewHolder viewHolder, int position) {
        final PatrimonioBean item = patrimonios.get(position);
        viewHolder.itemView.setTag(item);
        viewHolder.art.setText(item.getPed_artcod());
        viewHolder.cant.setText(String.valueOf(item.getPed_cantidad()));
        viewHolder.date.setText(item.getPed_fecemi());
    }

    @Override
    public int getItemCount() {
        return patrimonios.size();
    }

    static class PedidoViewHolder extends RecyclerView.ViewHolder {
        protected TextView art;
        protected TextView cant;
        protected TextView date;
        public View layout;

        public PedidoViewHolder(View v) {
            super(v);
            layout = v;
            this.art = (TextView) v.findViewById(R.id.molde_patri_art);
            this.cant = (TextView) v.findViewById(R.id.molde_patri_cant);
            this.date = (TextView) v.findViewById(R.id.molde_patri_date);
        }
    }

}
