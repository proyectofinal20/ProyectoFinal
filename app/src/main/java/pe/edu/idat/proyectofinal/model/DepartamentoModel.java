package pe.edu.idat.proyectofinal.model;

public abstract class DepartamentoModel {
    int dep_id;
    String dep_desc;

    public DepartamentoModel() {
    }

    public DepartamentoModel(int dep_id, String dep_desc) {
        this.dep_id = dep_id;
        this.dep_desc = dep_desc;
    }

    public int getDep_id() {
        return dep_id;
    }

    public void setDep_id(int dep_id) {
        this.dep_id = dep_id;
    }

    public String getDep_desc() {
        return dep_desc;
    }

    public void setDep_desc(String dep_desc) {
        this.dep_desc = dep_desc;
    }
}
