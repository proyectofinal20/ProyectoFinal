package pe.edu.idat.proyectofinal.model;

public abstract class ProvinciaModel {
    int prov_id;
    String prov_desc;

    public ProvinciaModel() {
    }

    public ProvinciaModel(int prov_id, String prov_desc) {
        this.prov_id = prov_id;
        this.prov_desc = prov_desc;
    }

    public int getProv_id() {
        return prov_id;
    }

    public void setProv_id(int prov_id) {
        this.prov_id = prov_id;
    }

    public String getProv_desc() {
        return prov_desc;
    }

    public void setProv_desc(String prov_desc) {
        this.prov_desc = prov_desc;
    }
}
