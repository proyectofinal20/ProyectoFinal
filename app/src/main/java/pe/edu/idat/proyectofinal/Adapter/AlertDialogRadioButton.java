package pe.edu.idat.proyectofinal.Adapter;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.Button;

public class AlertDialogRadioButton {
    AlertDialog alertDialog1;
    public void DialogRadioButton(final Context context, final String nro, final String nro2) {

        CharSequence[] values = {" Celular 1 ", " Celular 2 "};

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);

        builder.setTitle("Elige uno para comunicarte");

        builder.setSingleChoiceItems(values, -1, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {

                switch (item) {
                    case 0:
                        Intent i = new Intent(Intent.ACTION_CALL);
                        i.setData(Uri.parse("tel:"+nro.toString()));
                        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        context.startActivity(i);
                        break;
                    case 1:
                        Intent in = new Intent(Intent.ACTION_CALL);
                        in.setData(Uri.parse("tel:"+nro2.toString()));
                        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        context.startActivity(in);
                        break;
                }
                alertDialog1.dismiss();
            }
        });
        alertDialog1 = builder.create();
        alertDialog1.show();

    }
//condicion
    public void DialogRadioButtonCodiNumero(final Context context, final String nro, final String nro2,final String nomcliente) {

        CharSequence[] values = {" Celular 1 ", " Celular 2 "};

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);

        builder.setTitle("Elige uno teléfono para enviar un mensaje");

        builder.setSingleChoiceItems(values, -1, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {

                switch (item) {
                    case 0:
                       Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + nro.toString()));
                        intent.putExtra("sms_body", "Saludos Cordiales Sr(a): "+nomcliente.toString()+"\n");
                        context.startActivity(intent);

                        break;
                    case 1:
                        Intent intent1 = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + nro.toString()));
                        intent1.putExtra("sms_body", "Saludos Cordiales Sr(a): "+nomcliente.toString()+"\n");
                        context.startActivity(intent1);
                        break;
                }
                alertDialog1.dismiss();
            }
        });
        alertDialog1 = builder.create();
        alertDialog1.show();

    }


    //Alert Dialog mensaje
    public void DialogRadioButtonMessage(final Context context, final String nro, final String nom_cliente) {

        CharSequence[] values = {" Mensaje de Texto ", " Whatsapp "};

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);

        builder.setTitle("Elige uno para comunicarte");

        builder.setSingleChoiceItems(values, -1, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {

                switch (item) {
                    case 0:
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + nro.toString()));
                        intent.putExtra("sms_body", "Saludos Cordiales Sr(a):"+nom_cliente.toString()+"\n");
                        context.startActivity(intent);
                        break;
                    case 1:

                        break;
                }
                alertDialog1.dismiss();
            }
        });
        alertDialog1 = builder.create();
        alertDialog1.show();

    }
}
