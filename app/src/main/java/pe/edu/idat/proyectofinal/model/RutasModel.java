package pe.edu.idat.proyectofinal.model;

public abstract class RutasModel {
    String rt_codigo;
    String rt_clicod;
    Double rt_latitud;
    Double rt_longitud;
    String rt_direc;
    String rt_movil1;
    String rt_movil2;

    public RutasModel() {
    }

    public RutasModel(String rt_codigo, String rt_clicod, Double rt_latitud, Double rt_longitud, String rt_direc, String rt_movil1, String rt_movil2) {
        this.rt_codigo = rt_codigo;
        this.rt_clicod = rt_clicod;
        this.rt_latitud = rt_latitud;
        this.rt_longitud = rt_longitud;
        this.rt_direc = rt_direc;
        this.rt_movil1 = rt_movil1;
        this.rt_movil2 = rt_movil2;
    }

    public String getRt_codigo() {
        return rt_codigo;
    }

    public void setRt_codigo(String rt_codigo) {
        this.rt_codigo = rt_codigo;
    }

    public String getRt_clicod() {
        return rt_clicod;
    }

    public void setRt_clicod(String rt_clicod) {
        this.rt_clicod = rt_clicod;
    }

    public Double getRt_latitud() {
        return rt_latitud;
    }

    public void setRt_latitud(Double rt_latitud) {
        this.rt_latitud = rt_latitud;
    }

    public Double getRt_longitud() {
        return rt_longitud;
    }

    public void setRt_longitud(Double rt_longitud) {
        this.rt_longitud = rt_longitud;
    }

    public String getRt_direc() {
        return rt_direc;
    }

    public void setRt_direc(String rt_direc) {
        this.rt_direc = rt_direc;
    }

    public String getRt_movil1() {
        return rt_movil1;
    }

    public void setRt_movil1(String rt_movil1) {
        this.rt_movil1 = rt_movil1;
    }

    public String getRt_movil2() {
        return rt_movil2;
    }

    public void setRt_movil2(String rt_movil2) {
        this.rt_movil2 = rt_movil2;
    }
}
