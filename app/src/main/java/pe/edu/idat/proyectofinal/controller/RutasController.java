package pe.edu.idat.proyectofinal.controller;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import pe.edu.idat.proyectofinal.util.URLWebService;

public class RutasController {

    final static String strURLRest = URLWebService.URL+"/ProyectoFinal/Ruta/Rest/";
    URL url = null;
    HttpURLConnection httpURLConnection = null;
    OutputStreamWriter outputStreamWriter = null;
    JSONObject jsonObject = null;

    public RutasController() {
    }

    public JSONObject httpURLConnectionServer(String strURL,int intMethod, JSONObject jsonObject){
        try {
            url = new URL(strURL);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestProperty("Accept", "application/json");
            httpURLConnection.setUseCaches(false);
            //httpURLConnection.setRequestProperty("Accept-Encoding", "gzip");
            httpURLConnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            httpURLConnection.setConnectTimeout(10000); // 10 Seg
            httpURLConnection.setReadTimeout(15000); // 15 Seg

            switch (intMethod){
                case 0:
                    httpURLConnection.setRequestMethod("GET");
                    break;

            }

            InputStreamReader inputStreamReader = new InputStreamReader(httpURLConnection.getInputStream(), "UTF-8");
            BufferedReader in = new BufferedReader(inputStreamReader);
            String strLine;
            StringBuffer stringBuffer = new StringBuffer();

            while ((strLine = in.readLine()) != null) {
                stringBuffer.append(strLine);
            }
            Log.i("FINAL",stringBuffer.toString());
            if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                jsonObject = new JSONObject(stringBuffer.toString());
            }

        }catch (Exception e){
            e.printStackTrace();
            try {
                jsonObject.put("status", false);
                jsonObject.put("message", e.getMessage());
            }catch(JSONException ex){
                Log.e("ERROR",ex.getMessage());
            }
        }
        return jsonObject;
    }

    public JSONObject ListarRutasReparto(){
        return httpURLConnectionServer(strURLRest+"/Listado",0,new JSONObject());
    }
}
