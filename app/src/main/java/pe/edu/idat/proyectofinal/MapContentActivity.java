package pe.edu.idat.proyectofinal;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MapContentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_content);
    }

    public void backpressed(){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
