package pe.edu.idat.proyectofinal.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import pe.edu.idat.proyectofinal.ActivityPatrimonioCliente;
import pe.edu.idat.proyectofinal.R;
import pe.edu.idat.proyectofinal.bean.ClienteBean;

public class PatrimonioListAdapter extends RecyclerView.Adapter<PatrimonioListAdapter.PatrimonioViewHolder> {
    //
    private Context context;
    private List<ClienteBean> clientes;
    private RecyclerView mRecyclerV;


    public PatrimonioListAdapter(List<ClienteBean> items, Context contexto, RecyclerView recyclerView) {
        this.context = contexto;
        this.clientes = items;
        this.mRecyclerV = recyclerView;
    }
    //

    @Override
    public PatrimonioViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.molde_cliente, viewGroup, false);
        return new PatrimonioViewHolder(v);
    }

    @Override
    public void onBindViewHolder(PatrimonioViewHolder viewHolder, int position) {
        final ClienteBean item = clientes.get(position);
        viewHolder.itemView.setTag(item);
        //Picasso.with(context).load(item.getAnun_image()).fit().centerCrop().into(viewHolder.imganunimage);
        //  viewHolder.notiimage.setImageDrawable(item.getTitulo());
        viewHolder.itemView.setTag(item);
        viewHolder.nombre.setText(item.getCli_nombre());
        viewHolder.direccion.setText(item.getCli_direc());
        viewHolder.img_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialogRadioButton alertDialogRadioButton = new AlertDialogRadioButton();
                alertDialogRadioButton.DialogRadioButton(context,item.getCli_movil1(),item.getCli_movil2());
            }
        });
        viewHolder.img_msm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialogRadioButton alertDialogRadioButton = new AlertDialogRadioButton();
                alertDialogRadioButton.DialogRadioButtonCodiNumero(context,item.getCli_movil1(),item.getCli_movil2(),item.getCli_nombre());
            }
        });
        viewHolder.img_mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Intent.ACTION_SEND);
                String[] recipients={item.getCli_email()};
                intent.putExtra(Intent.EXTRA_EMAIL, recipients);
                intent.putExtra(Intent.EXTRA_SUBJECT,"Informe");
                intent.putExtra(Intent.EXTRA_TEXT,"Saludos cordiales Sr(a): "+item.getCli_nombre()+" "+item.getCli_apellido()+"\n");
                intent.setType("text/html");
                intent.setPackage("com.google.android.gm");
                context.startActivity(Intent.createChooser(intent, "Send mail"));
            }
        });
        viewHolder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ActivityPatrimonioCliente.class);
                intent.putExtra(Intent.EXTRA_TEXT, item);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return clientes.size();
    }
    //viewholder vista
    static class PatrimonioViewHolder extends RecyclerView.ViewHolder {
        //Campos respectivos de los item del molde
        //protected ImageView imganunimage;
        protected TextView nombre;
        protected TextView direccion;
        protected ImageView img_mail;
        protected ImageView img_msm;
        protected ImageView img_call;
        public View layout;

        public PatrimonioViewHolder(View v) {
            super(v);
            layout = v;
            this.nombre = (TextView) v.findViewById(R.id.molde_cli_nombre);
            this.direccion = (TextView) v.findViewById(R.id.molde_cli_direccion);
            this.img_mail = (ImageView) v.findViewById(R.id.molde_cli_mail);
            this.img_msm = (ImageView) v.findViewById(R.id.molde_cli_message);
            this.img_call = (ImageView) v.findViewById(R.id.molde_clie_call);
        }
    }




}

