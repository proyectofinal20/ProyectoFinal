package pe.edu.idat.proyectofinal.model;

public abstract class DistritoModel {
    int dist_id;
    String dist_desc;
    String dist_cod;

    public DistritoModel() {
    }

    public DistritoModel(int dist_id, String dist_desc, String dist_cod) {
        this.dist_id = dist_id;
        this.dist_desc = dist_desc;
        this.dist_cod = dist_cod;
    }

    public int getDist_id() {
        return dist_id;
    }

    public void setDist_id(int dist_id) {
        this.dist_id = dist_id;
    }

    public String getDist_desc() {
        return dist_desc;
    }

    public void setDist_desc(String dist_desc) {
        this.dist_desc = dist_desc;
    }

    public String getDist_cod() {
        return dist_cod;
    }

    public void setDist_cod(String dist_cod) {
        this.dist_cod = dist_cod;
    }
}
