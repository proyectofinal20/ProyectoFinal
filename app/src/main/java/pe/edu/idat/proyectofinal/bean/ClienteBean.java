package pe.edu.idat.proyectofinal.bean;

import java.io.Serializable;

import pe.edu.idat.proyectofinal.model.ClienteModel;

public class ClienteBean extends ClienteModel{
    public ClienteBean() {
    }

    public ClienteBean(String cli_codigo, String cli_apellido, String cli_nombre, String cli_movil1, String cli_movil2, String cli_email, String cli_direc, String cli_ubigeo, String cli_imagen, Double cli_latitud, Double cli_longitud, int cli_estado) {
        super(cli_codigo, cli_apellido, cli_nombre, cli_movil1, cli_movil2, cli_email, cli_direc, cli_ubigeo, cli_imagen, cli_latitud, cli_longitud, cli_estado);
    }
}
