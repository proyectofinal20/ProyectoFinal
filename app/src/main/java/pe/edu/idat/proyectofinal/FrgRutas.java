package pe.edu.idat.proyectofinal;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pe.edu.idat.proyectofinal.Adapter.RutaListAdapter;
import pe.edu.idat.proyectofinal.bean.RutaBean;
import pe.edu.idat.proyectofinal.controller.RutaController;

public class FrgRutas extends Fragment {
    View view;
    RecyclerView rc;
    RutaController rutaController = new RutaController();
    public FrgRutas() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_frg_rutas, container, false);
        rc = (RecyclerView) v.findViewById(R.id.rvListRutas);
        ConnectivityManager connMgr = (ConnectivityManager)
                getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            new ListarRutas().execute();
        } else {
            final AlertDialog.Builder builder = new AlertDialog.
                    Builder(getActivity());
            builder.create();
            builder.setTitle("Alerta");
            builder.setMessage(R.string.msgAlertaInternet);
            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });
            builder.create().show();
        }
        return v;
    }

    @Override
    public void onStart() {
        new ListarRutas().execute();
        super.onStart();
    }

    private class ListarRutas extends AsyncTask<String, Void, JSONObject> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(getActivity(), "Cargando", "espere por favor");
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            return rutaController.ListarRutasDetalleAll();
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            progressDialog.dismiss();
            ArrayList<RutaBean> arrayListrutas = new ArrayList<>();
            try {
                if(jsonObject.getBoolean("status") && jsonObject.has("data")) {
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    Log.i("JSON ",jsonArray.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        String ruta_date = jsonArray.getJSONObject(i).getString("ruta_fecha");
                        int ruta_nro = jsonArray.getJSONObject(i).getInt("ruta_nrocliente");

                        //CREAR ADAPTER

                        arrayListrutas.add(new RutaBean("","","","",0,"","",0,ruta_date, ruta_nro));
                    }
                }
                RutaListAdapter ad = new RutaListAdapter(arrayListrutas,getActivity(),rc);
                //RECYCLERVIEW

                rc.setHasFixedSize(true);

                RecyclerView.LayoutManager ly = new LinearLayoutManager(getActivity());
                rc.setLayoutManager(ly);

                rc.setAdapter(ad);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

}
