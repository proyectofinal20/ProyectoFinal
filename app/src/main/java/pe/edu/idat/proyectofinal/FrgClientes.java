package pe.edu.idat.proyectofinal;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pe.edu.idat.proyectofinal.Adapter.ClienteListAdapter;
import pe.edu.idat.proyectofinal.bean.ClienteBean;
import pe.edu.idat.proyectofinal.controller.ClienteController;

public class FrgClientes extends Fragment {
    View view;
    RecyclerView rc;
    ClienteController clienteController = new ClienteController();

    public static FrgClientes newInstance() {
        FrgClientes fragment = new FrgClientes();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_frg_clientes, container, false);
        FloatingActionButton fabnewclient = v.findViewById(R.id.fab_newClient);
        fabnewclient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),NewClientActivity.class);
                startActivity(intent);
            }
        });

        rc = (RecyclerView) v.findViewById(R.id.rvListClientes);
        ConnectivityManager connMgr = (ConnectivityManager)
                getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            ListarClientes myAsync = new ListarClientes();
            myAsync.execute();
        } else {
            final AlertDialog.Builder builder = new AlertDialog.
                    Builder(getActivity());
            builder.create();
            builder.setTitle("Alerta");
            builder.setMessage(R.string.msgAlertaInternet);
            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });
            builder.create().show();
        }
        return v;
    }

    @Override
    public void onStart() {
        ListarClientes myAsync = new ListarClientes();
        myAsync.execute();
        super.onStart();
    }

    class ListarClientes extends AsyncTask<String, Void, JSONObject> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(getActivity(), "Cargando", "espere por favor");
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            return clienteController.ListarClienteAll();
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            progressDialog.dismiss();
            ArrayList<ClienteBean> arrayListclientes = new ArrayList<>();
            try {
                if(jsonObject.getBoolean("status") && jsonObject.has("data")) {
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    Log.i("JSON ",jsonArray.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        String cli_codigo = jsonArray.getJSONObject(i).getString("cli_codigo");
                        String cli_apellido = jsonArray.getJSONObject(i).getString("cli_apellido");
                        String cli_nombre = jsonArray.getJSONObject(i).getString("cli_nombre");
                        String cli_movil1 = jsonArray.getJSONObject(i).getString("cli_movil1");
                        String cli_movil2 = jsonArray.getJSONObject(i).getString("cli_movil2");
                        String cli_email = jsonArray.getJSONObject(i).getString("cli_email");
                        String cli_direc = jsonArray.getJSONObject(i).getString("cli_direc");
                        String cli_ubigeo = jsonArray.getJSONObject(i).getString("cli_ubigeo");
                        String cli_imagen = jsonArray.getJSONObject(i).getString("cli_imagen");
                        Double cli_latitud = jsonArray.getJSONObject(i).getDouble("cli_latitud");
                        Double cli_longitud = jsonArray.getJSONObject(i).getDouble("cli_longitud");
                        int cli_estado = jsonArray.getJSONObject(i).getInt("cli_estado");

                        //CREAR ADAPTER

                        arrayListclientes.add(new ClienteBean(cli_codigo, cli_apellido, cli_nombre, cli_movil1, cli_movil2, cli_email, cli_direc, cli_ubigeo, cli_imagen,cli_latitud,cli_longitud,cli_estado));
                    }
                }
                ClienteListAdapter ad = new ClienteListAdapter(arrayListclientes,getActivity(),rc);
                //RECYCLERVIEW

                rc.setHasFixedSize(true);

                RecyclerView.LayoutManager ly = new LinearLayoutManager(getActivity());
                rc.setLayoutManager(ly);

                rc.setAdapter(ad);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

}
