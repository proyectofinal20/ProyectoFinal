package pe.edu.idat.proyectofinal.bean;

import pe.edu.idat.proyectofinal.model.RutaModel;

public class RutaBean extends RutaModel {

    public RutaBean() {
    }

    public RutaBean(String rep_codigo, String rep_fecemi, String rep_observ, String dist_codigo, int rep_estado, String repd_repcod, String repd_pedcod, int repd_estado, String ruta_date, int ruta_nrocliente) {
        super(rep_codigo, rep_fecemi, rep_observ, dist_codigo, rep_estado, repd_repcod, repd_pedcod, repd_estado, ruta_date, ruta_nrocliente);
    }
}
