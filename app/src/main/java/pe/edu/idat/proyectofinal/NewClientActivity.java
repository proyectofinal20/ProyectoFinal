package pe.edu.idat.proyectofinal;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.jackandphantom.circularimageview.CircleImage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import pe.edu.idat.proyectofinal.DataBase.DataAccess;
import pe.edu.idat.proyectofinal.DataBase.DataBaseHelper;
import pe.edu.idat.proyectofinal.bean.DepartamentoBean;
import pe.edu.idat.proyectofinal.bean.DistritoBean;
import pe.edu.idat.proyectofinal.bean.ProvinciaBean;
import pe.edu.idat.proyectofinal.controller.ClienteController;
import pe.edu.idat.proyectofinal.sharedpreferences.MarkerMapClass;

public class NewClientActivity extends AppCompatActivity {
EditText newclient_direccion;
EditText client_apellido,client_nombres,client_num1,client_num2,client_email,client_direccion;
CircleImage client_img;
Button client_register;
    String mktitulo = "";
    Double mklat;
    Double mklong;
    Spinner client_depa,client_provi,client_distri;
    private static final int PICK_IMAGE = 100;
    Uri imageUri;
    AlertDialog alertDialog1;
    ClienteController clienteController=new ClienteController();
    DataAccess databaseAccess;
    HashMap<Integer, DepartamentoBean> spinnerIdDepa;
    HashMap<Integer,ProvinciaBean> spinnerIdProvi;
    HashMap<Integer,DistritoBean> spinnerIdDist;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_client);

        ((ImageView)findViewById(R.id.cli_img_ubicacion)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NewClientActivity.this,MapContentActivity.class);
                startActivity(intent);
            }
        });
        Referencias();

    }

    @Override
    protected void onStart() {
        Referencias();
        super.onStart();
    }

    private void Referencias(){

        client_apellido = (EditText) findViewById(R.id.newclient_txt_apellidos);
        client_nombres = (EditText) findViewById(R.id.newclient_txt_nombres);
        client_num1 = (EditText) findViewById(R.id.newclient_txt_num1);
        client_num2 = (EditText) findViewById(R.id.newclient_txt_num2);
        client_email = (EditText) findViewById(R.id.newclient_txt_email);
        client_direccion = (EditText) findViewById(R.id.newclient_txt_direccion);
        client_register = (Button) findViewById(R.id.newclient_btn_register);
        client_img = (CircleImage) findViewById(R.id.newclient_img);
        client_depa = (Spinner) findViewById(R.id.newclient_sp_depa);
        client_provi = (Spinner) findViewById(R.id.newclient_sp_provi);
        client_distri = (Spinner) findViewById(R.id.newclient_sp_distri);

        JSONObject obj = null;

        try {
                obj = new JSONObject(MarkerMapClass.getPreferences(NewClientActivity.this, "dataMarkerUser"));
            Log.i("MARCADOR LATITUD",obj.toString());
                mktitulo = obj.getString("marker_title");
                mklat = Double.valueOf(obj.getString("marker_latitud"));
                mklong = Double.valueOf(obj.getString("marker_longitud"));
                Log.i("MARCADOR LATITUD",mktitulo.toString());
            } catch(JSONException e){
                e.printStackTrace();
            }

        //parseo
        newclient_direccion = (EditText) findViewById(R.id.newclient_txt_direccion);
        newclient_direccion.setText(mktitulo.toString());
        ClickView();
        SpinnersDatabase();

      client_depa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
          @Override
          public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
              SpinnerProvincia(spinnerIdDepa.get(client_depa.getSelectedItemPosition()).getDep_id());
          }

          @Override
          public void onNothingSelected(AdapterView<?> adapterView) {

          }
      });

      client_provi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
          @Override
          public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
              SpinnerDistrito(spinnerIdProvi.get(client_provi.getSelectedItemPosition()).getProv_id());
          }

          @Override
          public void onNothingSelected(AdapterView<?> adapterView) {

          }
      });

    }
    private void ClickView(){
        client_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CreateAlertDialogWithRadioButtonGroup() ;
            }
        });

        client_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new insCliente().execute();
            }
        });


    }

       public void CreateAlertDialogWithRadioButtonGroup(){
        CharSequence[] values = {" Tomar foto "," De galería "};

        AlertDialog.Builder builder = new AlertDialog.Builder(NewClientActivity.this);

        builder.setTitle("¿De donde obtendrás la imagen?");

        builder.setSingleChoiceItems(values, -1, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {

                switch(item)
                {
                    case 0:
                        OpenCamera();
                        break;
                    case 1:
                        openGallery();
                        break;
                }
                alertDialog1.dismiss();
            }
        });
        alertDialog1 = builder.create();
        alertDialog1.show();

    }

    //Funcion para abrir la camara del dispositivo.
    private void OpenCamera(){
        Intent cameraIntent = new Intent(
                android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        //Creamos una carpeta en la memeria del terminal
        File imagesFolder = new File(
                Environment.getExternalStorageDirectory(), "ProyectoFinal");
        imagesFolder.mkdirs();
        //añadimos el nombre de la imagen
        File image = new File(imagesFolder, "foto.jpg");
        Uri uriSavedImage = Uri.fromFile(image);
        //Le decimos al Intent que queremos grabar la imagen
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
        //Lanzamos la aplicacion de la camara con retorno (forResult)
        startActivityForResult(cameraIntent, 1);
    }

    //Funcion para abrir la galeria del dispositivo
    private void openGallery(){
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(resultCode == RESULT_OK && requestCode == PICK_IMAGE){
            imageUri = data.getData();
            client_img.setImageURI(imageUri);

        }

        if (requestCode == 1 && resultCode == RESULT_OK) {
            //Creamos un bitmap con la imagen recientemente
            //almacenada en la memoria
            Bitmap bMap = BitmapFactory.decodeFile(
                    Environment.getExternalStorageDirectory()+
                            "/ProyectoFinal/"+"foto.jpg");
            //Añadimos el bitmap al imageView para
            //mostrarlo por pantalla
            // Dividimos el ancho final por el ancho de la imagen original
            client_img.setImageBitmap(bMap);
        }
    }

    //ASYNCTASK
    private class insCliente extends AsyncTask<String, Void, JSONObject> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            //TODO - MOSTRAR CUADRO DE DIALOGO EMERGENTE
            dialog = new ProgressDialog(NewClientActivity.this);
            dialog.setTitle("Registro");
            dialog.setMessage("Cargando Cliente...");
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... argumento) {
            Log.i("VER ERRORES","INICIAMOS ASYNTASK");
            //TODO - CREAR REQUEST JSON
            JSONObject jsonObject = null;
             try {
                 jsonObject = new JSONObject();
                jsonObject.put("cli_apellido", client_apellido.getText().toString());
                jsonObject.put("cli_nombre", client_nombres.getText().toString());
                jsonObject.put("cli_movil1", client_num1.getText().toString());
                jsonObject.put("cli_movil2", client_num2.getText().toString());
                jsonObject.put("cli_email", client_email.getText().toString());
                jsonObject.put("cli_direc", client_direccion.getText().toString());
                jsonObject.put("cli_ubigeo", spinnerIdDist.get(client_distri.getSelectedItemPosition()).getDist_cod());
                jsonObject.put("cli_imagen", "");
                jsonObject.put("cli_latitud", mklat.toString());
                jsonObject.put("cli_longitud", mklong.toString());
                jsonObject.put("cli_estado", 1);

            }catch (JSONException e){
                e.printStackTrace();
            }
            Log.i("ENVIANDO DATOS",jsonObject.toString());

            return clienteController.insClienteAll(jsonObject);
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            if(this.dialog.isShowing()){this.dialog.dismiss();}
            try {
                //TODO - MOSTRAR KEY MESSAGE
                if(jsonObject.getBoolean("status")) {
                    String mensaje = jsonObject.getString("message").toString();
                    Toast.makeText(getApplicationContext(), mensaje.toString(), Toast.LENGTH_LONG).show();
                    onBackPressed();
                }else{
                    Toast.makeText(getApplicationContext(), "ERROR EN EL SERVIDOR", Toast.LENGTH_LONG).show();
                }

            }catch (Exception e){
                Log.e("ERROR",e.getMessage());
            }

        }

    }

   private void SpinnersDatabase(){
       DataBaseHelper db = new DataBaseHelper(NewClientActivity.this);

       ArrayList<DepartamentoBean> departamentoArrayList;
       departamentoArrayList = db.selDepartamento1();
       //SPINNER ID
       String[] spinnerArray = new String[departamentoArrayList.size()];
       spinnerIdDepa = new HashMap<Integer, DepartamentoBean>();
       for (int i = 0; i < departamentoArrayList.size(); i++)
       {
           spinnerIdDepa.put(i,departamentoArrayList.get(i));
           spinnerArray[i] = departamentoArrayList.get(i).getDep_desc();
       }


       // Creating adapter for spinner
       ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
               android.R.layout.simple_spinner_item, spinnerArray);

       // Drop down layout style - list view with radio button
       dataAdapter
               .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

       // attaching data adapter to spinner
       client_depa.setAdapter(dataAdapter);

   }

    private void SpinnerProvincia(int id){
        DataBaseHelper db = new DataBaseHelper(NewClientActivity.this);

        ArrayList<ProvinciaBean> provinciaArrayList;
        provinciaArrayList = db.selProvincia(id);
        //SPINNER ID
        String[] spinnerArray = new String[provinciaArrayList.size()];
        spinnerIdProvi = new HashMap<Integer, ProvinciaBean>();
        for (int i = 0; i < provinciaArrayList.size(); i++)
        {
            spinnerIdProvi.put(i,provinciaArrayList.get(i));
            spinnerArray[i] = provinciaArrayList.get(i).getProv_desc();
        }


        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, spinnerArray);

        // Drop down layout style - list view with radio button
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        client_provi.setAdapter(dataAdapter);


    }

    private void SpinnerDistrito(int id){
        DataBaseHelper db = new DataBaseHelper(NewClientActivity.this);

        ArrayList<DistritoBean> distritoArrayList;
        distritoArrayList = db.selDistrito(id);
        //SPINNER ID
        String[] spinnerArray = new String[distritoArrayList.size()];
        spinnerIdDist = new HashMap<Integer, DistritoBean>();
        for (int i = 0; i < distritoArrayList.size(); i++)
        {
            spinnerIdDist.put(i,distritoArrayList.get(i));
            spinnerArray[i] = distritoArrayList.get(i).getDist_desc();
        }


        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, spinnerArray);

        // Drop down layout style - list view with radio button
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        client_distri.setAdapter(dataAdapter);
    }

    private String convertimage(){
        String image;
        if(client_img.getDrawable() == null) {
            image = "";
        }else{
            //CONVIERTE LA IMAGEN DEL IMAGEVIEW A BASE64 String
            int ancho = 140;
            int alto = 220;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ancho, alto);
            client_img.setLayoutParams(params);
            client_img.buildDrawingCache();
            BitmapDrawable drawable = (BitmapDrawable) client_img.getDrawable();
            Bitmap bitmap = drawable.getBitmap();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            //float proporcion = 220 / (float) mta_img.getWidth();

//multiplicamos el resultado por la altura de la imagen original
// para conseguir la altura final
            Bitmap imagenFinal = Bitmap.createScaledBitmap(bitmap,ancho,alto,false);
            imagenFinal.compress(Bitmap.CompressFormat.PNG,100,bos);
            byte[] bb = bos.toByteArray();
            image = Base64.encodeToString(bb,Base64.DEFAULT);
        }
        return image;
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
}
