package pe.edu.idat.proyectofinal.bean;

import pe.edu.idat.proyectofinal.model.ArticuloModel;

public class ArticuloBean extends ArticuloModel {
    public ArticuloBean() {
    }

    public ArticuloBean(String art_codigo, String art_descripcion, Double art_precunidad, int art_estado) {
        super(art_codigo, art_descripcion, art_precunidad, art_estado);
    }
}
