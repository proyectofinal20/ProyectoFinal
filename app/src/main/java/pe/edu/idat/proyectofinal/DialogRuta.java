package pe.edu.idat.proyectofinal;

import android.annotation.SuppressLint;
import android.app.DialogFragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

import pe.edu.idat.proyectofinal.Adapter.AlertDialogRadioButton;
import pe.edu.idat.proyectofinal.Adapter.PatrimonioDetalleListAdapter;
import pe.edu.idat.proyectofinal.bean.PatrimonioDetalleBean;
import pe.edu.idat.proyectofinal.controller.PatrimonioController;
import pe.edu.idat.proyectofinal.controller.PatrimonioDetalleController;

import static android.content.ContentValues.TAG;

public class DialogRuta extends BottomSheetDialog implements View.OnClickListener {

    private TextView txt_codped,txt_direccion,txt_total;
    String txt_telf1,txt_telf2,codpedido;
    private RecyclerView rc;
    private Button btnRebote, btnEntrega;
    private ImageView imgcall;
    private Context context;
    PatrimonioController patrimonioController=new PatrimonioController();
    PatrimonioDetalleController patrimonioDetalleController=new PatrimonioDetalleController();

    @SuppressLint("StaticFieldLeak")
    private static DialogRuta instance;

    public static DialogRuta getInstance(@NonNull Context context) {
        return instance == null ? new DialogRuta(context) : instance;
    }

    public DialogRuta(@NonNull Context context) {
        super(context);
        this.context = context;
        create();
    }

    public void create() {
        View bottomSheetView = getLayoutInflater().inflate(R.layout.fragment_frg_ruta_dialog, null);
        setContentView(bottomSheetView);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
        BottomSheetBehavior.BottomSheetCallback bottomSheetCallback = new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // do something
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // do something
            }
        };

        btnRebote = bottomSheetView.findViewById(R.id.ruta_btn_rebote);
        btnEntrega = bottomSheetView.findViewById(R.id.ruta_btn_entrega);
        imgcall = bottomSheetView.findViewById(R.id.ruta_img_call);
        txt_codped = bottomSheetView.findViewById(R.id.ruta_txt_codped);
        txt_total = bottomSheetView.findViewById(R.id.ruta_txt_total);
        txt_direccion = bottomSheetView.findViewById(R.id.ruta_txt_direccion);
        rc = bottomSheetView.findViewById(R.id.ruta_rv_Articulos);


        btnRebote.setOnClickListener(this);
        btnEntrega.setOnClickListener(this);
        imgcall.setOnClickListener(this);

    }

    public void txt_codped(String txt_codped) {
        this.txt_codped.setText(txt_codped);
        new ListarArticulos(txt_codped.toString()).execute();
    }

    public void txt_direccion(String txt_direccion) {
        this.txt_direccion.setText(txt_direccion);
    }


    public void telf1(String txt_telf) {
        this.txt_telf1 = txt_telf.toString();
    }

    public void telf2(String txt_telf2) {
        this.txt_telf2 = txt_telf2.toString();
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ruta_btn_rebote:

                break;
            case R.id.ruta_btn_entrega:
                //hide();
                break;
            case R.id.ruta_img_call:
                AlertDialogRadioButton alertDialogRadioButton = new AlertDialogRadioButton();
                alertDialogRadioButton.DialogRadioButton(context,txt_telf1.toString(),txt_telf2.toString());
             break;
        }
    }

    private class ListarArticulos extends AsyncTask<String, Void, JSONObject> {
        // PROPIEDADES DE CLASES
        String cod;

        //CONSTRUCTOR
        public ListarArticulos(String cod) {
            this.cod = cod;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            return patrimonioDetalleController.ListarPatrimonioDetalleAll(cod.toString());
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            ArrayList<PatrimonioDetalleBean> arrayListpatrimonio= new ArrayList<>();
            try {
                Double totalNeto = 0.0;
                if(jsonObject.getBoolean("status") && jsonObject.has("data")) {
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    Log.i("JSON ",jsonArray.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        String pedd_codigo = jsonArray.getJSONObject(i).getString("pedd_codigo");
                        String pedd_artcod = jsonArray.getJSONObject(i).getString("pedd_artcod");
                        int pedd_cantidad = jsonArray.getJSONObject(i).getInt("pedd_cantidad");
                        Double pedd_precio = jsonArray.getJSONObject(i).getDouble("pedd_precio");
                        int pedd_estado = jsonArray.getJSONObject(i).getInt("pedd_estado");
                        totalNeto = totalNeto + pedd_precio;
                        //CREAR ADAPTER
                        arrayListpatrimonio.add(new PatrimonioDetalleBean(pedd_codigo,pedd_artcod,pedd_cantidad,pedd_precio,pedd_estado));
                    }
                }
                DecimalFormat df = new DecimalFormat("#.00");
                txt_total.setText("Total: "+String.valueOf(df.format(totalNeto)));

                PatrimonioDetalleListAdapter ad = new PatrimonioDetalleListAdapter(arrayListpatrimonio,context,rc);
                //RECYCLERVIEW

                rc.setHasFixedSize(true);

                RecyclerView.LayoutManager ly = new LinearLayoutManager(context);
                rc.setLayoutManager(ly);

                rc.setAdapter(ad);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
    /*
    //UPDATE
    private class updEstado extends AsyncTask<String, Void, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            JSONObject jsonParam = null;
            try {
                JSONObject obj = new JSONObject(json.toString());
                jsonParam = new JSONObject();
                jsonParam.put("ped_codigo",obj.getString("ped_codigo"));
                jsonParam.put("ped_fecemi",obj.getString("ped_fecemi"));
                jsonParam.put("ped_clicod",obj.getString("ped_clicod"));
                jsonParam.put("ped_estado",obj.getString("ped_estado"));

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("params",jsonParam.toString());

            //return patrimonioController.updPatrimonioAll(jsonParam);
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            try {
                //TODO - MOSTRAR KEY MESSAGE
                if(jsonObject.getBoolean("status")) {
                    Toast.makeText(context, "Lista Actualizada", Toast.LENGTH_LONG).show();
                }

            }catch (Exception e){
                Log.e("ERROR",e.getMessage());
            }


        }
    }
    */
}