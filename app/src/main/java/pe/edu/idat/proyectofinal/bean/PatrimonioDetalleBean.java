package pe.edu.idat.proyectofinal.bean;

import pe.edu.idat.proyectofinal.model.PatrimonioDetalleModel;

public class PatrimonioDetalleBean extends PatrimonioDetalleModel {
    public PatrimonioDetalleBean() {
    }

    public PatrimonioDetalleBean(String ped_codigo, String ped_artcod, int ped_cantidad, Double ped_precio, int ped_estadodet) {
        super(ped_codigo, ped_artcod, ped_cantidad, ped_precio, ped_estadodet);
    }
}
