package pe.edu.idat.proyectofinal.bean;

import pe.edu.idat.proyectofinal.model.DistritoModel;

public class DistritoBean extends DistritoModel {

    public DistritoBean() {
    }

    public DistritoBean(int dist_id, String dist_desc, String dist_cod) {
        super(dist_id, dist_desc, dist_cod);
    }
}
