package pe.edu.idat.proyectofinal;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pe.edu.idat.proyectofinal.Adapter.PedidoListAdapter;
import pe.edu.idat.proyectofinal.bean.ArticuloBean;
import pe.edu.idat.proyectofinal.bean.ClienteBean;
import pe.edu.idat.proyectofinal.bean.PatrimonioBean;
import pe.edu.idat.proyectofinal.controller.ArticuloController;
import pe.edu.idat.proyectofinal.controller.PatrimonioController;
import pe.edu.idat.proyectofinal.controller.PatrimonioDetalleController;

public class ActivityPatrimonioCliente extends AppCompatActivity implements View.OnClickListener {
    private FloatingActionButton fab,fab1,fab2;
    private Animation fab_open,fab_close,rotate_forward,rotate_backward;
    private Boolean isFabOpen = false;
    private TextView patri_fullname,patri_address,patri_desart;
    private EditText patri_codarticulo,patri_cantidad;
    ImageView patri_search,patri_agregar;
    ArticuloController articuloController=new ArticuloController();
    PatrimonioController patrimonioController=new PatrimonioController();
    PatrimonioDetalleController patrimonioDetalleController=new PatrimonioDetalleController();
    JSONArray jsonObjectArticulos = new JSONArray();
    String art_codigo = null;
    Double art_precunidad = null;
    String cli_id = null;
    RecyclerView rc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patrimonio_cliente);
        fab = (FloatingActionButton)findViewById(R.id.fab);
        fab1 = (FloatingActionButton)findViewById(R.id.fab1);
        fab2 = (FloatingActionButton)findViewById(R.id.fab2);
        //Obteniendo las animacion de la carpeta anim y asignando al flaoting button principal
        fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_backward);
        fab.setOnClickListener(this);
        fab1.setOnClickListener(this);
        fab2.setOnClickListener(this);
        References();
        ObtenerIntent();
    }

    @Override
    protected void onStart() {
        new ListarPatrimonios().execute();
        super.onStart();
    }

    private void References(){
        patri_fullname = (TextView) findViewById(R.id.patri_txt_fullname);
        patri_address = (TextView) findViewById(R.id.patri_txt_address);
        patri_codarticulo = (EditText) findViewById(R.id.patri_txt_codart);
        patri_desart = (TextView) findViewById(R.id.patri_txt_desart);
        patri_cantidad = (EditText) findViewById(R.id.patri_txt_cantidad);
        patri_agregar = (ImageView) findViewById(R.id.patri_btn_agregar);
        patri_search = (ImageView) findViewById(R.id.patri_img_search);
        rc = (RecyclerView) findViewById(R.id.patri_rv_patrimonio);
        patri_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new ListarArticulo().execute();
            }
        });
        patri_agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new insPatrimonio().execute();
            }
        });
    }

    private void ObtenerIntent(){
        //Recuperando el id del item seleccionado desde listview
        ClienteBean clienteBean;
        Intent intent = getIntent();
        if(intent.hasExtra(Intent.EXTRA_TEXT)) {
            clienteBean = (ClienteBean) intent.getSerializableExtra(Intent.EXTRA_TEXT);
            patri_fullname.setText(clienteBean.getCli_apellido()+" "+clienteBean.getCli_nombre());
            patri_address.setText(clienteBean.getCli_direc().toString());
            cli_id = clienteBean.getCli_codigo().toString();
        }
    }

    public void animateFAB(){
        if(isFabOpen){

            fab.startAnimation(rotate_backward);
            fab1.startAnimation(fab_close);
            fab2.startAnimation(fab_close);
            fab1.setClickable(false);
            fab2.setClickable(false);
            isFabOpen = false;
            Log.d("Raj", "close");

        } else {

            fab.startAnimation(rotate_forward);
            fab1.startAnimation(fab_open);
            fab2.startAnimation(fab_open);
            fab1.setClickable(true);
            fab2.setClickable(true);
            isFabOpen = true;
            Log.d("Raj","open");

        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.fab:
                //El boton principal con este id activara la animacion al momento de dar click
                animateFAB();
                break;
            case R.id.fab1:
                new insPatrimonioAll().execute();
                new insPatrimonioDetalleAll().execute();
                break;
            case R.id.fab2:
                jsonObjectArticulos = new JSONArray();
                patri_cantidad.setText("");
                new ListarPatrimonios().execute();
                break;
        }
    }

    private class ListarArticulo extends AsyncTask<String, Void, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            return articuloController.ListarArticuloAll(patri_codarticulo.getText().toString());
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            //ArrayList<ArticuloBean> arrayListarticulo = new ArrayList<>();
            try {
                if(jsonObject.getBoolean("status") && jsonObject.has("data")) {
                    //JSONArray jsonArray = jsonObject.getJSONArray("data");
                    String data = jsonObject.getString("data");
                    //String desc = jsonObject.getString("art_precunidad");
                JSONObject obj = new JSONObject(data);
                String id = obj.getString("art_descripcion");
                    Log.i("JSON ", id);

                    art_codigo = obj.getString("art_codigo");
                    //String art_descripcion = jsonArray.getJSONObject(i).getString("art_descripcion");
                    art_precunidad = obj.getDouble("art_precunidad");
                    //int art_estado = jsonArray.getJSONObject(i).getInt("art_estado");

                    patri_desart.setText(id.toString());
                }else if (jsonObject.getBoolean("status")==false){
                    Toast.makeText(ActivityPatrimonioCliente.this,"Producto no encontrado",Toast.LENGTH_LONG).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    //ASYNCTASK
    private class insPatrimonio extends AsyncTask<String, Void, JSONObject> {
        ProgressDialog dialog;
        JSONObject jsonObject;

        @Override
        protected void onPreExecute() {
            //TODO - MOSTRAR CUADRO DE DIALOGO EMERGENTE
            dialog = new ProgressDialog(ActivityPatrimonioCliente.this);
            dialog.setTitle("Registro");
            dialog.setMessage("Cargando Patrimonio...");
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... argumento) {
            Log.i("VER ERRORES","INICIAMOS ASYNTASK");
            //TODO - CREAR REQUEST JSON
            //JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject();
                jsonObject.put("pedd_artcod", art_codigo);
                jsonObject.put("pedd_cantidad", Integer.parseInt(patri_cantidad.getText().toString()));
                Double total = art_precunidad * Integer.parseInt(patri_cantidad.getText().toString());
                jsonObject.put("pedd_precio", total);
                jsonObject.put("pedd_estadodet",1);
                jsonObjectArticulos.put(jsonObject);

            }catch (JSONException e){
                e.printStackTrace();
            }
            Log.i("ENVIANDO DATOS",jsonObjectArticulos.toString());
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            if(this.dialog.isShowing()){this.dialog.dismiss();}
            new ListarPatrimonios().execute();
            patri_cantidad.setText("");
                    Toast.makeText(getApplicationContext(), "ARTICULO AÑADIDO", Toast.LENGTH_SHORT).show();

        }
    }

    class ListarPatrimonios extends AsyncTask<String, Void, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            ArrayList<PatrimonioBean> arrayListpatrimonio = new ArrayList<>();
            try {
                    JSONArray jsonArray = jsonObjectArticulos;
                    Log.i("JSON ",jsonArray.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        String ped_fecemi = "04/09/2018";
                        String pedd_artcod = jsonArray.getJSONObject(i).getString("pedd_artcod");
                        int pedd_cantidad = jsonArray.getJSONObject(i).getInt("pedd_cantidad");

                        //CREAR ADAPTER

                        arrayListpatrimonio.add(new PatrimonioBean("", ped_fecemi, "", 0, pedd_artcod, pedd_cantidad,0.0,0));
                    }
                PedidoListAdapter ad = new PedidoListAdapter(arrayListpatrimonio,ActivityPatrimonioCliente.this,rc);
                //RECYCLERVIEW

                rc.setHasFixedSize(true);

                RecyclerView.LayoutManager ly = new LinearLayoutManager(ActivityPatrimonioCliente.this);
                rc.setLayoutManager(ly);

                rc.setAdapter(ad);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    //SINCRONIZAR CON WEB SERVICE
    //ASYNCTASK
    private class insPatrimonioDetalleAll extends AsyncTask<String, Void, JSONObject> {
        ProgressDialog dialog;
        JSONObject messagejson;
        @Override
        protected void onPreExecute() {
            //TODO - MOSTRAR CUADRO DE DIALOGO EMERGENTE
            dialog = new ProgressDialog(ActivityPatrimonioCliente.this);
            dialog.setTitle("Registro");
            dialog.setMessage("Cargando Detalles...");
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... argumento) {
            Log.i("VER ERRORES","INICIAMOS ASYNTASK");
            //TODO - CREAR REQUEST JSON
            //JSONObject jsonObject = null;
            for (int i = 0; i < jsonObjectArticulos.length();i++) {
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("pedd_artcod", jsonObjectArticulos.getJSONObject(i).getString("pedd_artcod"));
                    jsonObject.put("pedd_cantidad", jsonObjectArticulos.getJSONObject(i).getInt("pedd_cantidad"));
                    jsonObject.put("pedd_precio", jsonObjectArticulos.getJSONObject(i).getDouble("pedd_precio"));
                    jsonObject.put("pedd_estadodet", jsonObjectArticulos.getJSONObject(i).getString("pedd_estadodet"));
                    Log.i("ENVIANDO DATOS",jsonObject.toString());
                    patrimonioDetalleController.insPatrimonioDetalleAll(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            try {
                messagejson = new JSONObject().put("message",jsonObjectArticulos.length() + " Registro Guardados");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return messagejson;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            if(this.dialog.isShowing()){this.dialog.dismiss();}
            try {
                //TODO - MOSTRAR KEY MESSAGE
                    String mensaje = jsonObject.getString("message").toString();
                    Toast.makeText(getApplicationContext(), mensaje.toString(), Toast.LENGTH_LONG).show();
                onBackPressed();
            }catch (Exception e){
                Log.e("ERROR",e.getMessage());
            }

        }
    }

    private class insPatrimonioAll extends AsyncTask<String, Void, JSONObject> {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            //TODO - MOSTRAR CUADRO DE DIALOGO EMERGENTE
            dialog = new ProgressDialog(ActivityPatrimonioCliente.this);
            dialog.setTitle("Registro");
            dialog.setMessage("Cargando Patrimonio...");
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... argumento) {
            Log.i("VER ERRORES","INICIAMOS ASYNTASK");
            //TODO - CREAR REQUEST JSON
            JSONObject json = null;
                try {
                    json = new JSONObject();
                    json.put("ped_clicod", cli_id.toString());
                    json.put("ped_estado", 1);
                    Log.i("ENVIANDO DATOS",json.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            return patrimonioController.insPatrimonioAll(json);
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            if(this.dialog.isShowing()){this.dialog.dismiss();}
            try {
                //TODO - MOSTRAR KEY MESSAGE
                String mensaje = jsonObject.getString("message").toString();
                Log.i("CORRECTO", mensaje.toString());
            }catch (Exception e){
                Log.e("ERROR",e.getMessage());
            }

        }
    }
}
