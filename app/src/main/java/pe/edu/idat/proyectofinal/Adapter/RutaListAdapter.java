package pe.edu.idat.proyectofinal.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import pe.edu.idat.proyectofinal.DialogRuta;
import pe.edu.idat.proyectofinal.R;
import pe.edu.idat.proyectofinal.RutaDetalleActivity;
import pe.edu.idat.proyectofinal.bean.RutaBean;

public class RutaListAdapter extends RecyclerView.Adapter<RutaListAdapter.RutaViewHolder> {
    //
    private Context context;
    private List<RutaBean> rutas;
    private RecyclerView mRecyclerV;


    public RutaListAdapter(List<RutaBean> items, Context contexto, RecyclerView recyclerView) {
        this.context = contexto;
        this.rutas = items;
        this.mRecyclerV = recyclerView;
    }
    //

    @Override
    public RutaViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.molde_ruta, viewGroup, false);
        return new RutaViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RutaViewHolder viewHolder, int position) {
        final RutaBean item = rutas.get(position);
        viewHolder.itemView.setTag(item);
        viewHolder.img.setText(nameDay(String.valueOf(item.getRuta_date())));
        viewHolder.date.setText(String.valueOf(item.getRuta_date()));
        viewHolder.nro.setText(String.valueOf(item.getRuta_nrocliente())+" Clientes");
        viewHolder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, RutaDetalleActivity.class);
                intent.putExtra("rutaFecha", item.getRuta_date());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return rutas.size();
    }

    static class RutaViewHolder extends RecyclerView.ViewHolder {
        protected TextView img;
        protected TextView date;
        protected TextView nro;
        public View layout;

        public RutaViewHolder(View v) {
            super(v);
            layout = v;
            this.img = (TextView) v.findViewById(R.id.molde_ruta_image);
            this.date = (TextView) v.findViewById(R.id.molde_ruta_fecha);
            this.nro = (TextView) v.findViewById(R.id.molde_ruta_nro);
        }
    }

    private String nameDay(String date){
        String nameDay=null;
        try {
            SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
            Date dt1 = format1.parse(date);
            DateFormat format2 = new SimpleDateFormat("EEEE");
            String finalDay = format2.format(dt1);
            nameDay = String.valueOf(finalDay).substring(0,1).toUpperCase();
        }catch (Exception e){e.printStackTrace();}
        return nameDay;
    }


}
