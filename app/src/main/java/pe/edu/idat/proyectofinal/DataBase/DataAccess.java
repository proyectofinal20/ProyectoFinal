package pe.edu.idat.proyectofinal.DataBase;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;

import pe.edu.idat.proyectofinal.bean.DepartamentoBean;

public class DataAccess {
    Context context;
    public void DataAccess(Context context){
        this.context = context;
    }
    DataBaseHelper db = new DataBaseHelper(context);
    public ArrayList<DepartamentoBean> selDepartamento(){
        Log.i("DATABASE LOG:","INICIAMOS");
        try {
            ArrayList<DepartamentoBean> notas = new ArrayList<DepartamentoBean>();
            db.createDataBase();
            db.openDataBase();
            Cursor cursor = DataBaseHelper.myDataBase.rawQuery("select * from departamento",null);
            if(cursor.moveToFirst()) {
                do {
                    DepartamentoBean dep = new DepartamentoBean();
                    dep.setDep_id(cursor.getInt(0));
                    dep.setDep_desc(cursor.getString(1));
                    notas.add(dep);
                    Log.i("DATABASE LOG:",cursor.getString(1));
                } while (cursor.moveToNext());
            }
            db.close();
            return notas;
        }catch (Exception e){
            return  null;
        }
    }


}
