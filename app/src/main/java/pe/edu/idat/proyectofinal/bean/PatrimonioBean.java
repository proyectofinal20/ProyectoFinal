package pe.edu.idat.proyectofinal.bean;

import pe.edu.idat.proyectofinal.model.PatrimonioModel;

public class PatrimonioBean extends PatrimonioModel {

    public PatrimonioBean() {
    }

    public PatrimonioBean(String ped_codigo, String ped_fecemi, String ped_clicod, int ped_estado, String ped_artcod, int ped_cantidad, Double ped_precio, int ped_estadodet) {
        super(ped_codigo, ped_fecemi, ped_clicod, ped_estado, ped_artcod, ped_cantidad, ped_precio, ped_estadodet);
    }
}
