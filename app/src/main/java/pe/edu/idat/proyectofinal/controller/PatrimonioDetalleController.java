package pe.edu.idat.proyectofinal.controller;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import pe.edu.idat.proyectofinal.util.URLWebService;

public class PatrimonioDetalleController {
    final static String strURLRest = URLWebService.URL+"/ProyectoFinal/PedidoDetalle/Rest";
    URL url = null;
    HttpURLConnection httpURLConnection = null;
    OutputStreamWriter outputStreamWriter = null;
    JSONObject jsonObject = null;

    public PatrimonioDetalleController() {
    }

    public JSONObject httpURLConnectionServer(String strURL,int intMethod, JSONObject jsonObject){
        try {
            url = new URL(strURL);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestProperty("Accept", "application/json");
            httpURLConnection.setUseCaches(false);
            //httpURLConnection.setRequestProperty("Accept-Encoding", "gzip");
            httpURLConnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            httpURLConnection.setConnectTimeout(10000); // 10 Seg
            httpURLConnection.setReadTimeout(15000); // 15 Seg

            switch (intMethod){
                case 0:
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setDoOutput(true);

                    outputStreamWriter = new OutputStreamWriter(httpURLConnection.getOutputStream(),"UTF-8");
                    outputStreamWriter.write(jsonObject.toString());
                    outputStreamWriter.flush();
                    outputStreamWriter.close();
                    break;
                case 1:
                    httpURLConnection.setRequestMethod("GET");
                    break;
            }

            InputStreamReader inputStreamReader = new InputStreamReader(httpURLConnection.getInputStream(), "UTF-8");
            BufferedReader in = new BufferedReader(inputStreamReader);
            String strLine;
            StringBuffer stringBuffer = new StringBuffer();

            while ((strLine = in.readLine()) != null) {
                stringBuffer.append(strLine);
            }
            Log.i("PROYECTO",stringBuffer.toString());
            if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                jsonObject = new JSONObject(stringBuffer.toString());
            }

        }catch (Exception e){
            e.printStackTrace();
            try {
                jsonObject.put("status", false);
                jsonObject.put("message", e.getMessage());
            }catch(JSONException ex){
                Log.e("ERROR",ex.getMessage());
            }
        }
        return jsonObject;
    }

    public JSONObject insPatrimonioDetalleAll(JSONObject jsonObject){
        return httpURLConnectionServer(strURLRest,0,jsonObject);
    }

    public JSONObject ListarPatrimonioDetalleAll(String id){
        return httpURLConnectionServer(strURLRest+"/"+id,1,new JSONObject());
    }
}
