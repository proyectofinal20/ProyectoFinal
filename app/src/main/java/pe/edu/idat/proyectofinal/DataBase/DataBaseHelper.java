package pe.edu.idat.proyectofinal.DataBase;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import pe.edu.idat.proyectofinal.bean.DepartamentoBean;
import pe.edu.idat.proyectofinal.bean.DistritoBean;
import pe.edu.idat.proyectofinal.bean.ProvinciaBean;

public class DataBaseHelper extends SQLiteOpenHelper {

    //The Android's default system path of your application database.
    private static String DB_PATH = "";
    private static String DB_NAME = "dbubigeo.sqlite";
    public  static SQLiteDatabase myDataBase;
    private final Context myContext;

    /**
     * Constructor
     * Takes and keeps a reference of the passed context in order to access to the application assets and resources.
     *
     * @param context
     */
    public DataBaseHelper(Context context) {
        super(context, DB_NAME, null, 1);
        DB_PATH="/data/data/" + context.getPackageName() + "/databases/";
        this.myContext = context;
    }

    /**
     * Creates a empty database on the system and rewrites it with your own database.
     */
    public void createDataBase() throws IOException {
        boolean dbExist = checkDataBase();

        if (dbExist) {
            //do nothing - database already exist
        } else {
            //By calling this method and empty database will be created into the default system path
            //of your application so we are gonna be able to overwrite that database with our database.
            this.getReadableDatabase();

            try {
                copyDataBase();
            } catch (IOException e) {
                throw new Error("Error copying database");
            }
        }

    }
    /**
     * Check if the database already exist to avoid re-copying the file each time you open the application.
     * @return true if it exists, false if it doesn't
     */
    private boolean checkDataBase(){
        SQLiteDatabase checkDB = null;

        try{
            String myPath = DB_PATH + DB_NAME;
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
            Log.i("EXISTE SQLITE","SI EXISTE");
        }catch(SQLiteException e){
            //database does't exist yet.
        }

        if(checkDB != null){
            checkDB.close();
        }
        return checkDB != null ? true : false;
    }

    /**
     * Copies your database from your local assets-folder to the just created empty database in the
     * system folder, from where it can be accessed and handled.
     * This is done by transfering bytestream.
     * */
    private void copyDataBase() throws IOException{

        //Open your local db as the input stream
        InputStream myInput = myContext.getAssets().open(DB_NAME);

        // Path to the just created empty db
        String outFileName = DB_PATH + DB_NAME;

        //Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);

        //transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer))>0){
            myOutput.write(buffer, 0, length);
        }

        //Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();

    }

    public void openDataBase() throws SQLException {
        //Open the database
        String myPath = DB_PATH + DB_NAME;
        myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
    }

    @Override
    public synchronized void close() {
        if(myDataBase != null)
            myDataBase.close();
        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    // Add your public helper methods to access and get content from the database.
    // You could return cursors by doing "return myDataBase.query(....)" so it'd be easy
    // to you to create adapters for your views.

    public ArrayList<DepartamentoBean> selDepartamento1(){
        try {
            Log.i("DATABASE LOG:","INICIAMOS");
            ArrayList<DepartamentoBean> notas = new ArrayList<DepartamentoBean>();
            SQLiteDatabase sqLiteDatabase = getWritableDatabase();
            Cursor cursor = sqLiteDatabase.rawQuery("select * from DEPARTAMENTO",null);
            if(cursor.moveToFirst()) {
                do {
                    DepartamentoBean nota = new DepartamentoBean();
                    nota.setDep_id(cursor.getInt(0));
                    nota.setDep_desc(cursor.getString(2));
                    notas.add(nota);
                } while (cursor.moveToNext());
            }
            Log.i("DATABASE LOG:","INTERMEDIO");
            sqLiteDatabase.close();
            return notas;
        }catch (Exception e){
            return  null;
        }
    }

    public ArrayList<ProvinciaBean> selProvincia(int id){
        try {
            Log.i("DATABASE LOG:","INICIAMOS");
            ArrayList<ProvinciaBean> notas = new ArrayList<ProvinciaBean>();
            SQLiteDatabase sqLiteDatabase = getWritableDatabase();
            Cursor cursor = sqLiteDatabase.rawQuery("select * from PROVINCIA WHERE PROV_IDDEPA ="+id,null);
            if(cursor.moveToFirst()) {
                do {
                    ProvinciaBean nota = new ProvinciaBean();
                    nota.setProv_id(cursor.getInt(0));
                    nota.setProv_desc(cursor.getString(1));
                    notas.add(nota);
                } while (cursor.moveToNext());
            }
            Log.i("DATABASE LOG:","INTERMEDIO");
            sqLiteDatabase.close();
            return notas;
        }catch (Exception e){
            return  null;
        }
    }

    public ArrayList<DistritoBean> selDistrito(int id){
        try {
            Log.i("DATABASE LOG:","INICIAMOS");
            ArrayList<DistritoBean> notas = new ArrayList<DistritoBean>();
            SQLiteDatabase sqLiteDatabase = getWritableDatabase();
            Cursor cursor = sqLiteDatabase.rawQuery("select * from DISTRITO WHERE DIST_IDPROV ="+id,null);
            if(cursor.moveToFirst()) {
                do {
                    DistritoBean nota = new DistritoBean();
                    nota.setDist_id(cursor.getInt(0));
                    nota.setDist_desc(cursor.getString(1));
                    nota.setDist_cod(cursor.getString(2));
                    notas.add(nota);
                } while (cursor.moveToNext());
            }
            Log.i("DATABASE LOG:","INTERMEDIO");
            sqLiteDatabase.close();
            return notas;
        }catch (Exception e){
            return  null;
        }
    }

}