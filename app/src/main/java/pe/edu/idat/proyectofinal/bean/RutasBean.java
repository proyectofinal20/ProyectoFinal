package pe.edu.idat.proyectofinal.bean;

import pe.edu.idat.proyectofinal.model.RutasModel;

public class RutasBean extends RutasModel {

    public RutasBean() {
    }

    public RutasBean(String rt_codigo, String rt_clicod, Double rt_latitud, Double rt_longitud, String rt_direc, String rt_movil1, String rt_movil2) {
        super(rt_codigo, rt_clicod, rt_latitud, rt_longitud, rt_direc, rt_movil1, rt_movil2);
    }
}
