package pe.edu.idat.proyectofinal.bean;

import pe.edu.idat.proyectofinal.model.DepartamentoModel;

public class DepartamentoBean extends DepartamentoModel {
    public DepartamentoBean() {
    }

    public DepartamentoBean(int dep_id, String dep_desc) {
        super(dep_id, dep_desc);
    }
}
